#!/usr/bin/env bash

if [ "$1" ];then
    Config_file=$1

else

    Config_file="Configuration_information_1 Configuration_information_2 Configuration_information_3 Configuration_information_4 Configuration_information_5 Configuration_information_6 Configuration_information_7"

fi

echo $Config_file;

for file_name in $Config_file

do

  gnome-terminal --tab --title="$file_name" --command="python3 rip_router.py "$file_name""

done