import sys
import socket
import threading
import time
import struct
import select
import random

HOST = '127.0.0.1'
#all time is devide by 3
PERIOD_SENT_TIME = 10
PERIOD_CHECK_TIMEOUT = 60
PEROPD_GARBAGE_COLLECTION = 40
# every 5s to start garbage collection and check timeout function
CHECK_TIME = 5
#b represent python type is integer standard size is 1, 
#h represent python type is integer standard size is 2 
#i represent python type is integer standard size is 4
HEAD_FROMAT = 'bbh'
ROUTER_TABLE_FROMAT = 'hhiiii'
#RIP package header, 2 represent response header
COMMAND = 2
#RIP protocol version 2 
VERSION = 2
MUST_ZERO = 0
#AF_INET
ADDRESS_FAMILY_IDENTIFIER = 2


def config_information():
    # Get parameter
    filename = "Conifguration_information/" + sys.argv[1]
    #print(filename)
    router_id = None
    input_ports = []
    output_ports = []
    output = []
    router_table = []
    isError = False
    # Base on parameter, read config file
    open_file = open(filename)
    lines = open_file.readlines()
    # Validation check for config file
    if len(lines) == 3:
        for i in range(len(lines)):
            if not isError:
                #splt
                datas = lines[i].strip().split(':')
                #length
                if len(datas) == 2:
                    if i == 0:
                        if datas[0] == 'router_id':
                            #get router id
                            router_id = datas[1]
                            print('router_id: ' + router_id)
                            if datas[1] == '' or 1 > int(datas[1]) or int(datas[1]) > 64000:
                                isError = True
                                print('Router id is out range !') 
                        else:
                            print('Missing router id !')
                            isError = True

                    if i == 1:
                        if datas[0] == 'input-ports':
                            # Get input ports
                            input_ports = datas[1].split(',')
                            print('input_ports: ' + str(input_ports))
                            for input_port in input_ports:
                                if input_port == '' or int(input_port) <1024 or int(input_port) > 64000:
                                    isError = True
                                    print('input ports is out range !')
                        else:
                            print('Missing input ports !')
                            isError = True

                    if i == 2:
                        if datas[0] == 'outputs':
                            # Get output ports
                            output_ports = datas[1].split(',')
                            print('output_ports: ' + str(output_ports))
                            for output_port in output_ports:
                                output = output_port.split('-')
                                if output == '' or 1024 > int(output[0]) or int(output[0]) > 64000:
                                    print('output port is out range !')
                                    isError = True
                                elif output == '' or int(output[1]) < 0 or int(output[1]) >= 16:
                                    print('Metric is out range !')
                                    isError = True
                                elif output == '' or 1 > int(output[2]) or int(output[2]) > 64000:
                                    print('Router id is out range !')
                                    isError = True
                            #is number
                        else:
                            print('Missing outputs !')
                            isError = True
                else:
                    print('Configuration information incomplete !')
                    isError = True
    else:
        print('Configuration information incomplete !')
        isError = True
    #initialize router table
    router_table.append({
        "destination": router_id,
        "metric": 0,
        "next_hop": None,
        "flag": False,
        "update_time": time.time()
    }) 

    return (router_table, router_id, input_ports, output_ports, isError)
    # Validation check for config file


def period_sent_router_table(router_table, output_ports, socket_item, period_sent_timer, router_id, event_type="PERIOD"):
    #conver router table data(header and detail) to binary

    socket_packet = struct.pack(HEAD_FROMAT,COMMAND, VERSION, int(router_id))

    for line in router_table:
        socket_packet += struct.pack(ROUTER_TABLE_FROMAT, ADDRESS_FAMILY_IDENTIFIER, MUST_ZERO, int(line["destination"]), MUST_ZERO, MUST_ZERO, int(line["metric"]))

    #send udp packet to neighbor router
    for output_id in output_ports:
        output = output_id.split('-')[0]

        socket_item.sendto(socket_packet, (HOST, int(output)))

    print('period_sent_router_table:')
    print_router_table(router_table, router_id)
    #create timmer when period send router table
    if event_type == "PERIOD":
        if period_sent_timer != None:
            period_sent_timer.cancel()
        
        #Set timer offset
        timer_offset =  random.randint(-5,5)
        timer_length = PERIOD_SENT_TIME + timer_offset
        period_sent_timer = threading.Timer(timer_length, period_sent_router_table,[router_table, output_ports, socket_item, period_sent_timer, router_id, "PERIOD"])
        period_sent_timer.start()
        #call socket to set packet

def Timeout(router_table, output_ports, socket_item, check_timeout, router_id):
    #if time is large than 180s the metric will be change to 16

    update_router_table = []
    isTimeout = False
    for line in router_table:
        if line["destination"] != router_id:
            update_time = line["update_time"]
            time_gap = time.time() - update_time
            if time_gap > PERIOD_CHECK_TIMEOUT:
                line["metric"] = 16
                line["update_time"] = time.time()
                update_router_table.append(line)
                isTimeout = True

    if isTimeout == True:
        print("Timeout: ")
        print_router_table(router_table, router_id)

        #Trigger update
        period_sent_router_table(update_router_table, output_ports, socket_item, None, router_id, "TIMEOUT")


    #start new timer
    if check_timeout != None:
        check_timeout.cancel()
    
    #Set timer offset
    timer_offset =  random.randint(-5,5)
    timer_length = CHECK_TIME + timer_offset
    check_timeout = threading.Timer(timer_length, Timeout, [router_table, output_ports, socket_item, check_timeout, router_id])
    check_timeout.start()
    

def garbage_collection(router_table, garbageCollection, router_id):
    #if metric = 16 and time more than 120s, the roter will be delete

    isRemoved = False
    for line in router_table:
        update_time = line["update_time"]
        if  line["metric"] == 16:
            if time.time() - update_time > PEROPD_GARBAGE_COLLECTION:
                router_table.remove(line)
                isRemoved = True
    if isRemoved == True:
        print('garbage_collection: ')
        print_router_table(router_table, router_id)

    if garbageCollection!= None:
        garbageCollection.cancel()
    
    #Set timer offset
    timer_offset =  random.randint(-5,5)
    timer_length = CHECK_TIME + timer_offset
    garbageCollection = threading.Timer(timer_length, garbage_collection, [router_table, garbageCollection, router_id])
    garbageCollection.start()


    

#check is destination exist in current routing table
def check_destination_exist(router_table, destination):
    isExist = False
    lineIndex = 0
    for i in range(len(router_table)):
        if router_table[i]["destination"] == str(destination):
            isExist = True
            lineIndex = i
            return (isExist, lineIndex)
    return (isExist, lineIndex)

def print_router_table(router_table, router_id):
    sorted_router = router_table

    for i in range(len(sorted_router)):
        if sorted_router[i]["destination"] != router_id:
            localtime = time.asctime( time.localtime(sorted_router[i]["update_time"]) )
            print('destination: {0}, metric: {1}, next_hop: {2}, flag: {3}, time_change_last_update: {4}'.format(str(sorted_router[i]["destination"]), str(sorted_router[i]["metric"]),str(sorted_router[i]["next_hop"]), str(sorted_router[i]["flag"]), str(int(time.time()) - int(sorted_router[i]["update_time"]))))
    
def main():
    router_table, router_id, input_ports, output_ports, isError = config_information()

    socket_table = []
    #Validation check is pass
    if isError == False:
        #Socket monitor input port
        for input_port in input_ports:
            socket_item = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            socket_item.bind((HOST, int(input_port)))
            socket_table.append(socket_item)

        #routing events
        #1. period sent router table
        readable, writable, exceptional = select.select([], [socket_table[0]], [])
        socket_send = writable[0]
        #Timer
        period_sent_timer = None
        period_sent_timer = threading.Timer(PERIOD_SENT_TIME, period_sent_router_table,[router_table, output_ports, socket_send, period_sent_timer, router_id, "PERIOD"])
        period_sent_timer.start()

        #2. periodic check timeout
        check_timeout = None
        check_timeout = threading.Timer(CHECK_TIME, Timeout, [router_table, output_ports, socket_send, check_timeout, router_id])
        check_timeout.start()

        #3. perioic garbage collection
        garbageCollection = None
        garbageCollection = threading.Timer(CHECK_TIME, garbage_collection, [router_table, garbageCollection, router_id])
        garbageCollection.start()

    #4. receive router table form neighbour router
    while True:
        readable, writable, exceptional = select.select(socket_table, [], socket_table)
        for data in readable:
            message = data.recvfrom(1024)
            # extract packet from message (list)
            packet = message[0]
            # unpack header data from packet
            # 0:4 because(command, 1), (version, 1), (router_id, 2)
            head_data = struct.unpack(HEAD_FROMAT, packet[0:4])
            if head_data[0] == 2 and head_data[1] == 2 and head_data[2] != None :
                #check command = 2
                #version = 2
                #router id != Null || ""
                for i in range(len(packet[4:]) // 20):
                    # each data has 20 bytes
                    detail_data = struct.unpack(ROUTER_TABLE_FROMAT, packet[4 + i * 20:4 + (i + 1) * 20])
                    if detail_data[2] != None and detail_data[5] <= 16:
                        #Only process data which destination is not myself
                        if detail_data[2] != router_id:
                            isExist, lineIndex = check_destination_exist(router_table, detail_data[2])
                            if isExist:
                                #next hop is same or not
                                if str(router_table[lineIndex]["next_hop"]) == str(head_data[2]):
                                    isDataChange = False
                                    #calculate metric and udpate router_table
                                    for output_id in output_ports:
                                        output = output_id.split('-')
                                        if output[2] == str(head_data[2]):
                                            new_metric = int(output[1]) + detail_data[5]
                                            if new_metric > 16:
                                                new_metric = 16
                                                
                                            if new_metric != router_table[lineIndex]["metric"]:
                                                isDataChange = True

                                            #if old metric is 16 it will be not change(wait for garbage collection) when metric less than 16 i weill be update
                                            if isDataChange == False and router_table[lineIndex]["metric"] == 16:
                                                pass
                                            else:
                                                router_table[lineIndex]["metric"] = new_metric
                                                router_table[lineIndex]["flag"] = True
                                                router_table[lineIndex]["update_time"] = time.time()

                                            if isDataChange == True:
                                                print('packet_receive, triggle update: ')
                                                print_router_table(router_table, router_id)

                                                #triggle update (poison reverse)
                                                update_router_table = [router_table[lineIndex]]
                                                period_sent_router_table(update_router_table, output_ports, socket_item, None, router_id, "TRIGGER")                                        
                                #next hot not the same, compare metric (udpate router_table by the metrice data)
                                else:
                                    for output_id in output_ports:
                                        output = output_id.split('-')
                                        if output[2] == str(head_data[2]):
                                            new_metric = int(output[1]) + detail_data[5]
                                            if new_metric < router_table[lineIndex]["metric"]:
                                                router_table[lineIndex]["metric"] = new_metric
                                                router_table[lineIndex]["flag"] = True
                                                router_table[lineIndex]["update_time"] = time.time()
                                                router_table[lineIndex]["next_hop"] = head_data[2]  

                                                print('packet_receive, triggle update: ')
                                                print_router_table(router_table, router_id)

                                                #triggle update (poison reverse)
                                                update_router_table = [router_table[lineIndex]]
                                                period_sent_router_table(update_router_table, output_ports, socket_item, None, router_id, "TRIGGER")


                            else:
                                #add in router_table
                                for output_id in output_ports:
                                        output = output_id.split('-')
                                        if output[2] == str(head_data[2]) and  detail_data[5] < 16:
                                            new_metric = int(output[1]) + detail_data[5]
                                            if new_metric > 16:
                                                new_metric = 16
                                            router_table.append({
                                                "destination": str(detail_data[2]),
                                                "metric": new_metric,
                                                "next_hop": output[2],
                                                "flag": False,
                                                "update_time": time.time()
                                            })

                                print('packet_receive, add new router data: ')
                                print_router_table(router_table, router_id)
        #pass
            else:
                #Packet Check fail
                print("Packet check error!")


if __name__ == '__main__':
    print('Start Programe')
    main()